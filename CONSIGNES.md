**Trier des listes**

Attention pour l'exercice vous devez réfléchir et coder par vous-même les algorithmes de tri. Vous ne devez pas utiliser de fonction ou package qui réalise le tri à votre place.

Déclarer la liste suivante avec les prénoms, noms et âges sous la forme d'une liste à 2 dimensions [[prénom1, nom1, age1], [prénom2, nom2, age2], [xxx, xxx, xxx], …]

    Pascal ROSE 43 ans
    Mickaël FLEUR 29 ans
    Henri TULIPE 35 ans
    Michel FRAMBOISE 35 ans
    Arthur PETALE 35 ans
    Michel POLLEN 50 ans
    Maurice FRAMBOISE 42 ans

Randomiser la liste avec la fonction random.shuffle(), consulter la doc python si nécessaire.

1) Afficher à l'utilisateur une liste_1 triée par ordre alphabétique des noms sous la forme :

Liste 1 : tri par ordre alphabétique des noms ##\
Prénom nom xx ans\
Prénom nom xx ans\
.\
.\
.\
'## FIN ##

2) Afficher à l'utilisateur une liste_2 triée par ordre alphabétique inversé des prénoms sous la même forme.

3) Afficher à l'utilisateur une liste_3 triée par ordre croissant des âges sous la même forme.

4) Afficher à l'utilisateur une liste_4 triée par ordre alphabétique des noms. Si des noms sont identiques, on les trie par ordre alphabétique des prénoms.\

**Pour aller plus loin**

1) Faire une recherche sur les fonctions inline en programmation.

Faire une recherche sur la fonction lambda et la fonction sorted() de la bibliothèque python standard.

Reproduisez l'énoncé en utilisant la fonction sorted() avec la fonction lambda.

Reproduisez l'énoncé en utilisant la fonction sorted() mais sans utiliser de fonction lambda.

Constatez la différence entre les deux solutions, et faites-en une déduction sur votre manière d'interpréter une fonction lambda.

2) Donnez l'énoncé de l'exercice à ChatGPT en lui demandant d'utiliser python et d'utiliser des fonctions lambda. Attention il faut au préalable enlever Maurice et les deux Michel de la liste des personnes.

Il doit normalement vous donner une réponse qui sera correcte par rapport à l'énoncé, mais qui deviendra erroné si on change la liste des personnes. Trouvez pourquoi ? En cas de blocage, comparez sa réponse avec la réponse que je propose pour l'exercice.

**Moralité : De grands pouvoirs impliquent de grandes responsabilités…**

3) Faire une fonction qui crée une liste random de 500 personnes avec des noms, prénoms et âges aléatoires. Chercher une librairie python qui produit des noms aléatoires. Au-delà de 500 personnes on risque d'atteindre la limite de récursivité. Si c'est le cas alors réduisez la taille de la liste. Calculez le temps de traitement de notre algorithme, par rapport à l'algorithme proposé par la fonction sorted() et comparez les deux. Si 1 000 personnes ne suffisent pas à avoir une différence significative entre les deux algorithmes, vous pouvez créer par exemple 10 listes random de 500 personnes. Attention il faut bien que les listes utilisées soient les mêmes pour les tests des deux algorithmes. Sinon on pourrait dire que l'un va plus vite peut-être car sa liste de personnes comporte moins de différences dans les noms.

Vous devez constater que la fonction sorted() propose un tri beaucoup plus efficace en terme de temps de calcul, que votre algorithme. Les ingénieurs qui travaillent sur les langages de programmation seront toujours bien meilleurs que vous pour concevoir les algorithmes les plus optimisés possibles. Donc d'une manière générale il faut toujours faire une recherche si l'algorithme ou la fonction qu'on cherche à faire n'existe pas déjà. Et si c'est le cas alors il faut l'utiliser pour garantir les meilleures performances dans notre application.
