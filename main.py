
lesgens = [["Pascal", "ROSE", 43],
           ["Mickaël", "FLEUR", 29],
           ["Henri", "TULIPE", 35],
           ["Michel", "FRAMBOISE", 35],
           ["Arthur", "PETALE", 35],
           ["Michel", "POLLEN", 50],
           ["Maurice", "FRAMBOISE", 42]]

'''random.shuffle(lesgens)'''

print(lesgens)


def trier_alpha(liste, index):
    """
    Permet de trier dans l'ordre alphabétique/numérique une liste selon un index donné
    :param liste: list in list
    :param index: from 0 to len(liste)
    :return: sorted list of list
    """
    for i in range(len(liste)):
        mini = i
        for j in range(i+1, len(liste)):
            if liste[j][index] < liste[mini][index]:
                mini = j
        liste[i], liste[mini] = liste[mini], liste[i]
    return liste


def print_list(list):
    """
    Permet la mise en forme du print
    :param list: list of list
    :return: fstring
    """
    for gens in list:
        print(f"{gens[0]} {gens[1]}, {gens[2]} ans.")
    print("## FIN ##")

print("## Liste 1 : tri par ordre alphabétique des noms ##")
liste_1 = trier_alpha(lesgens, 1)
print_list(liste_1)

print("\n## Liste 2 : tri par ordre alphabétique inversé des prénoms ##")
lesgens_prenoms = trier_alpha(lesgens, 0)
liste_2 = lesgens_prenoms[::-1]         # permet d'inverser l'ordre de la liste
print_list(liste_2)

print("\n## Liste 3 : tri par ordre croissant des âges ##")
liste_3 = trier_alpha(lesgens, 2)
print_list(liste_3)

print("\n## Liste 4 : tri par ordre alphabétique des noms puis des prénoms ##")
liste_4 = sorted(lesgens, key=lambda x: (x[1], x[0])) # façon "facile"
print_list(liste_4)
